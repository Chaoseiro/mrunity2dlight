﻿using UnityEngine;
using System.Collections;

namespace LikeASir
{
	[ExecuteInEditMode]
	public class MrLightObstacle : MonoBehaviour
	{
		public Vector2[] Points { get; protected set; }
		public Collider2D LightCollider;
		public bool IsCircleCollider { get; protected set; }
		// How much light can penetrate into the object
		//public float LightPenetration = 0.01f;

		// Use this for initialization
		void Awake()
		{
			if(LightCollider == null)
				LightCollider = GetComponent<Collider2D>();
			IsCircleCollider = LightCollider is CircleCollider2D;
		}

		public virtual Vector2[] GetPoints(Vector2 sourcePosition)
		{
			if (LightCollider == null)
				return null;

			Transform _transform = transform;
			if (LightCollider is CircleCollider2D)
			{
				IsCircleCollider = true;
				CircleCollider2D cc = LightCollider as CircleCollider2D;
				Vector2 center = cc.center + (Vector2)_transform.position;
				float radius = cc.radius* Mathf.Max(_transform.localScale.x, _transform.localScale.y);
				float distance = (sourcePosition - center).sqrMagnitude - radius * radius;
				if (distance < 0)
					return null;

				distance = Mathf.Sqrt(distance);
				Vector2[] intersections = FindCircleCircleIntersections(center, radius, sourcePosition, distance);
				if (intersections == null)
					return null;
				if (intersections.Length > 1)
				{
					float angle1 = Mathf.Atan2(intersections[0].y - center.y, intersections[0].x - center.x) * Mathf.Rad2Deg;
					float angle2 = Mathf.Atan2(intersections[1].y - center.y, intersections[1].x - center.x) * Mathf.Rad2Deg;

					if (angle1 > angle2)
						angle2 += 360;
					
					int steps = Mathf.FloorToInt((Mathf.Abs(angle2 - angle1)) / MrLightManager.Instance.CircleAngleResolution);

					Points = new Vector2[steps + 1];
					Vector2 direction = Vector2.zero;
					Points[0] = intersections[0];
					for (int i = 1; i < steps; i++)
					{
						if(angle1 < angle2)
							direction.Set(Mathf.Cos(Mathf.Deg2Rad * (angle1 + i * MrLightManager.Instance.CircleAngleResolution)), Mathf.Sin(Mathf.Deg2Rad * (angle1 + i * MrLightManager.Instance.CircleAngleResolution)));
						else
							direction.Set(Mathf.Cos(Mathf.Deg2Rad * (angle2 + i * MrLightManager.Instance.CircleAngleResolution)), Mathf.Sin(Mathf.Deg2Rad * (angle2 + i * MrLightManager.Instance.CircleAngleResolution)));
						Points[i] = center + direction * radius;
					}
					Points[steps] = intersections[1];
				}
				else
					Points = intersections;
			}
			else if (LightCollider is BoxCollider2D)
			{
				BoxCollider2D bc = LightCollider as BoxCollider2D;
				Points = new Vector2[4];
				Points[0] = new Vector2(
					_transform.position.x + bc.center.x - bc.size.x / 2.0f * _transform.localScale.x,
					_transform.position.y + bc.center.y - bc.size.y / 2.0f * _transform.localScale.y);
				Points[1] = new Vector2(
					_transform.position.x + bc.center.x + bc.size.x / 2.0f * _transform.localScale.x,
					_transform.position.y + bc.center.y - bc.size.y / 2.0f * _transform.localScale.y);
				Points[2] = new Vector2(
					_transform.position.x + bc.center.x + bc.size.x / 2.0f * _transform.localScale.x,
					_transform.position.y + bc.center.y + bc.size.y / 2.0f * _transform.localScale.y);
				Points[3] = new Vector2(
					_transform.position.x + bc.center.x - bc.size.x / 2.0f * _transform.localScale.x,
					_transform.position.y + bc.center.y + bc.size.y / 2.0f * _transform.localScale.y);
			}
			else if (LightCollider is EdgeCollider2D)
			{
				Points = (LightCollider as EdgeCollider2D).points;
				for (int i = 0; i < Points.Length; i++)
				{
					Points[i].x = Points[i].x * _transform.localScale.x + _transform.position.x;
					Points[i].y = Points[i].y * _transform.localScale.y + _transform.position.y;
				}
			}
			else if (LightCollider is PolygonCollider2D)
			{
				Points = (LightCollider as PolygonCollider2D).points;
				for (int i = 0; i < Points.Length; i++)
				{
					Points[i].x = Points[i].x * _transform.localScale.x + _transform.position.x;
					Points[i].y = Points[i].y * _transform.localScale.y + _transform.position.y;
				}
			}
			
			
			return Points;
		}

		private Vector2[] FindCircleCircleIntersections(
			Vector2 center0, float radius0,
			Vector2 center1, float radius1)
		{
			// Find the distance between the centers.
			//float dx = cx0 - cx1;
			//float dy = cy0 - cy1;
			float dist = (center0 - center1).magnitude;//Mathf.Sqrt(dx * dx + dy * dy);
			
			// See how many solutions there are.
			if (dist > radius0 + radius1)
			{
				// No solutions, the circles are too far apart.
				return null;
			}
			else if (dist < Mathf.Abs(radius0 - radius1))
			{
				// No solutions, one circle contains the other.
				return null;
			}
			else if ((dist == 0) && (radius0 == radius1))
			{
				// No solutions, the circles coincide.
				return null;
			}
			else
			{
				// Find a and h.
				float a = (radius0 * radius0 -
					radius1 * radius1 + dist * dist) / (2 * dist);
				float h = Mathf.Sqrt(radius0 * radius0 - a * a);

				// Find P2.
				float cx2 = center0.x + a * (center1.x - center0.x) / dist;
				float cy2 = center0.y + a * (center1.y - center0.y) / dist;

				// Get the points P3.
				Vector2[] p = new Vector2[(dist == radius0 + radius1) ? 1 : 2];
				p[0] = new Vector2(
					(float)(cx2 + h * (center1.y - center0.y) / dist),
					(float)(cy2 - h * (center1.x - center0.x) / dist));
				if(p.Length > 1)
					p[1] = new Vector2(
					(float)(cx2 - h * (center1.y - center0.y) / dist),
					(float)(cy2 + h * (center1.x - center0.x) / dist));

				return p;
			}
		}

		protected virtual void OnEnable()
		{
			MrLightManager.Instance.AddObstacle(this);
		}

		protected virtual void OnDisable()
		{
			if (MrLightManager.Instance != null)
			{
				MrLightManager.Instance.RemoveObstacle(this);
			}
		}
	}

	public static class Collider2DExtensions
	{
		public static Bounds GetBounds(this Collider2D coll)
		{
			Bounds bounds = new Bounds();

			if (coll is BoxCollider2D)
			{
				BoxCollider2D bc = coll as BoxCollider2D;
				bounds.center = bc.center;
				bounds.size = bc.size;
			}
			else if (coll is CircleCollider2D)
			{
				CircleCollider2D cc = coll as CircleCollider2D;
				bounds.center = cc.center;
				bounds.size = new Vector2(cc.radius, cc.radius);
			}
			else if (coll is EdgeCollider2D)
			{
				Vector3 min;
				Vector3 max;
				GetMinMaxPoints((coll as EdgeCollider2D).points, out min, out max);
				bounds.SetMinMax(min + coll.transform.position, max + coll.transform.position);
			}
			else if (coll is PolygonCollider2D)
			{
				Vector3 min;
				Vector3 max;
				GetMinMaxPoints((coll as PolygonCollider2D).points, out min, out max);
				bounds.SetMinMax(min + coll.transform.position, max + coll.transform.position);
			}

			return bounds;
		}
		private static void GetMinMaxPoints(Vector2[] points, out Vector3 min, out Vector3 max)
		{
			float minX = float.MaxValue;
			float maxX = float.MinValue;
			float minY = float.MaxValue;
			float maxY = float.MinValue;

			for (int i = 0; i < points.Length; i++)
			{
				Vector2 p = points[i];
				if (p.x < minX)
					minX = p.x;
				if (p.x > maxX)
					maxX = p.x;
				if (p.y < minY)
					minY = p.y;
				if (p.y > maxY)
					maxY = p.y;
			}

			min = new Vector3(minX, minY);
			max = new Vector3(maxX, maxY);
		}
	}
}