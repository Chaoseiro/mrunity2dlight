﻿using UnityEngine;
using System.Collections.Generic;

namespace LikeASir
{
	public class MrLightManager : Singleton<MrLightManager>
	{
		public float CircleAngleResolution = 1;
		public float CameraViewportExtension = 0.01f;
		public float RaycastDeltaAngle = 0.0001f;

		public bool Debug = false;

		protected List<MrLightSource> _lights;
		public List<MrLightSource> Lights
		{
			get
			{
				if (_lights == null)
					_lights = new List<MrLightSource>();
				return _lights;
			}
		}

		protected List<MrLightObstacle> _obstacles;
		public List<MrLightObstacle> Obstacles
		{
			get
			{
				if (_obstacles == null)
					_obstacles = new List<MrLightObstacle>();
				return _obstacles;
			}
		}

		public void AddLight(MrLightSource light)
		{
			if (!Lights.Contains(light))
			{
				Lights.Add(light);
			}
		}

		public void RemoveLight(MrLightSource light)
		{
			Lights.Remove(light);
		}

		public void AddObstacle(MrLightObstacle obstacle)
		{
			if (!Obstacles.Contains(obstacle))
				Obstacles.Add(obstacle);
		}

		public void RemoveObstacle(MrLightObstacle obstacle)
		{
			Obstacles.Remove(obstacle);
		}

		private void Initialize()
		{
			DontDestroyOnLoad(gameObject);
			_lights = new List<MrLightSource>();
			_obstacles = new List<MrLightObstacle>();
		}

		void Start()
		{
			Initialize();
		}

		void LateUpdate()
		{
			//UpdateLights();
		}
	}
}
