﻿using UnityEngine;
using System.Collections.Generic;

namespace LikeASir
{
	[ExecuteInEditMode]
	public class MrLightSource : MonoBehaviour
	{
		public LayerMask ObstacleLayers;

		public Color Color = Color.yellow;
		public Material Material;
		public int SortingLayerOrder;
		public int SortingLayer;

		protected Mesh _mesh;
		protected MeshFilter _meshFilter;
		protected MeshRenderer _meshRenderer;

		public Vector2 CachedPosition { get; protected set; }
		public MrLightCamera LightCamera;

		protected Color _previousColor;
		protected int _previousSortingOrder;
		protected int _previousSortingLayer;

		// Use this for initialization
		void Awake()
		{
			Initialize();
		}

		protected virtual void Initialize()
		{
			_meshFilter = GetComponent<MeshFilter>();
			if (_meshFilter == null)
			{
				_meshFilter = gameObject.AddComponent<MeshFilter>();
			}

			if (_mesh == null)
			{
				_mesh = new Mesh();
				_meshFilter.mesh = _mesh;
			}

			_meshRenderer = GetComponent<MeshRenderer>();
			if (_meshRenderer == null)
				_meshRenderer = gameObject.AddComponent<MeshRenderer>();

			_meshRenderer.receiveShadows = false;
			_meshRenderer.castShadows = false;

			CachedPosition = transform.position;
		}

		protected virtual void OnEnable()
		{
			MrLightManager.Instance.AddLight(this);
			if (_meshRenderer != null)
			{
				_meshRenderer.enabled = true;
			}
		}

		protected virtual void OnDisable()
		{
			if (MrLightManager.Instance != null)
			{
				MrLightManager.Instance.RemoveLight(this);
			}
			if (_meshRenderer != null)
			{
				_meshRenderer.enabled = false;
			}
		}

		protected virtual void LateUpdate()
		{
			CachedPosition = transform.position;
			DrawLight();
		}

		public virtual void DrawLight()
		{
			List<IntersectionPoint> intersectionPoints = CalculateIntersections();
			if (intersectionPoints == null)
				return;

			List<Vector3> meshVertices = new List<Vector3>();
			List<int> triangles = new List<int>();
			int currentVertexIndex = -1;
			int previousVertexIndex = -1;

			meshVertices.Add(Vector3.zero);
			previousVertexIndex = 0;
			for (int i = 0; i < intersectionPoints.Count; i++)
			{
				meshVertices.Add(intersectionPoints[i].Point - CachedPosition);
				previousVertexIndex = currentVertexIndex;
				currentVertexIndex = meshVertices.Count - 1;
				if (i > 0)
					AddTriangle(triangles, 0, currentVertexIndex, previousVertexIndex);
			}

			AddTriangle(triangles, 0, 1, currentVertexIndex);

			DeployMesh(meshVertices, triangles);
		}

		protected virtual void DeployMesh(List<Vector3> vertices, List<int> triangles)
		{
			_mesh.Clear();
			_mesh.vertices = vertices.ToArray();
			_mesh.triangles = triangles.ToArray();
			_mesh.RecalculateNormals();
			UpdateColor();
			UpdateUVs();
			UpdateSortingLayer();
			UpdateSortingOrder();
		}

		protected void AddTriangle(List<int> trianglesList, int index0, int index1, int index2)
		{
			trianglesList.Add(index0);
			trianglesList.Add(index1);
			trianglesList.Add(index2);
		}

		protected virtual void OnDrawGizmos()
		{
			if (MrLightManager.Instance == null)
				return;
			if (MrLightManager.Instance.Debug)
			{
				List<IntersectionPoint> intersectionPoints = CalculateIntersections();
				if (intersectionPoints == null)
					return;
				for (int i = 0; i < intersectionPoints.Count; i++)
				{
					Gizmos.DrawLine(CachedPosition, intersectionPoints[i].Point);
					Gizmos.DrawSphere(intersectionPoints[i].Point, 0.05f);
				}
			}
		}

		public virtual List<IntersectionPoint> CalculateIntersections()
		{
			if (MrLightManager.Instance == null)
				return null;

			// Raycast against each visible obstacle's points, then build the mesh using the intersections
			List<MrLightObstacle> obstacles = MrLightManager.Instance.Obstacles;

			List<Vector2> raycastPoints = new List<Vector2>();
			List<Vector2> raycastDirections = new List<Vector2>();
			List<IntersectionPoint> intersectionPoints = new List<IntersectionPoint>();

			RaycastHit2D hit2D;

			// add raycastPoints to camera borders
			if (LightCamera != null)
			{
				raycastPoints.AddRange(LightCamera.ViewportWorldCoordinates);
			}

			for (int i = 0; i < obstacles.Count; i++)
			{
				// if the obstacle is not visible to the camera, wont use it for raycasts
				if (LightCamera != null && !LightCamera.IsObstacleIsInView(obstacles[i]))
					continue;
				
				// Add object's points to raycast list
				Vector2[] points = obstacles[i].GetPoints(CachedPosition);
				if (points != null)
				{
					raycastPoints.AddRange(points);

					// We must include any intersection with camera borders
					for (int j = 1; j < points.Length; j++)
					{
						Vector2[] lineintersect = LightCamera.GetCollisionPointsWithViewBox(points[j - 1], points[j]);
						if (lineintersect != null)
						{
							raycastPoints.AddRange(lineintersect);
						}
					}
				}
			}

			// convert the raycast points into directions
			for (int i = 0; i < raycastPoints.Count; i++)
			{
				Vector2 dif = (raycastPoints[i] - CachedPosition);
				float angle = Mathf.Atan2(dif.y, dif.x);//Vector2.Angle(CachedPosition, raycastPoints[j]) * Mathf.Deg2Rad;
				raycastDirections.Add(new Vector2(Mathf.Cos(angle - MrLightManager.Instance.RaycastDeltaAngle), Mathf.Sin(angle - MrLightManager.Instance.RaycastDeltaAngle)));
				raycastDirections.Add(new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)));
				raycastDirections.Add(new Vector2(Mathf.Cos(angle + MrLightManager.Instance.RaycastDeltaAngle), Mathf.Sin(angle + MrLightManager.Instance.RaycastDeltaAngle)));
			}

			// now, finally, raycast in the directions and save the intersection points
			float _rayDistance = LightCamera.RayMaxNeededDistance();
			for (int j = 0; j < raycastDirections.Count; j++)
			{
				hit2D = Physics2D.Raycast(CachedPosition, raycastDirections[j], _rayDistance, ObstacleLayers);
				if (hit2D.collider != null)
				{
					Vector2 intersectPoint = hit2D.point;
					float intersectAngle = Mathf.Atan2(intersectPoint.y - CachedPosition.y, intersectPoint.x - CachedPosition.x);
					//MrLightObstacle obstacle = hit2D.collider.gameObject.GetComponent<MrLightObstacle>();
					//if (obstacle != null)
					//	intersectPoint += obstacle.LightPenetration * raycastDirections[j];
					intersectionPoints.Add(new IntersectionPoint(intersectPoint, intersectAngle));
				}
			}
			if (intersectionPoints.Count < 1)
				return null;

			// sort intersections by angle
			intersectionPoints.Sort(
				delegate(IntersectionPoint i1, IntersectionPoint i2)
				{
					return i1.Angle.CompareTo(i2.Angle);
				}
				);

			//return intersectionPoints;
			// now reduce intersection points by removing points that are too close
			List<IntersectionPoint> reducedIntersections = new List<IntersectionPoint>();
			reducedIntersections.Add(intersectionPoints[0]);
			for (int i = 1; i < intersectionPoints.Count; i++)
			{
				if ((intersectionPoints[i].Point - reducedIntersections[reducedIntersections.Count - 1].Point).sqrMagnitude < 0.0005f)
					continue;
				reducedIntersections.Add(intersectionPoints[i]);
			}

			return reducedIntersections;
		}

		protected void UpdateColor()
		{
			_previousColor = Color;

			Color32[] colors32 = new Color32[_mesh.vertices.Length];
			for (int i = 0; i < colors32.Length; i++)
			{
				colors32[i] = Color;
			}
			_mesh.colors32 = colors32;
		}

		protected void UpdateSortingLayer()
		{
			_meshRenderer.sortingLayerID = SortingLayer;
			_previousSortingLayer = SortingLayer;
		}

		protected void UpdateSortingOrder()
		{
			_meshRenderer.sortingOrder = SortingLayerOrder;
			_previousSortingOrder = SortingLayerOrder;
		}

		protected void UpdateUVs()
		{
			Vector2[] uvs = new Vector2[_mesh.vertices.Length];

			for (int i = 0; i < uvs.Length; i++)
			{
				float u = _mesh.vertices[i].x / GetMaxLightLength() / 2.0f + 0.5f;
				float v = _mesh.vertices[i].y / GetMaxLightLength() / 2.0f + 0.5f;

				u = Mathf.Clamp01(u);
				v = Mathf.Clamp01(v);
				
				uvs[i] = new Vector2(u, v);
			}
			_meshRenderer.sharedMaterial = Material;
			_mesh.uv = uvs;
		}

		protected virtual float GetMaxLightLength() 
		{
			return LightCamera.GetScreenSizeInWorld().x / 2.0f;
		}
	}

	public struct IntersectionPoint
	{
		public Vector2 Point;
		public float Angle;

		public IntersectionPoint(Vector2 point, float angle)
		{
			Point = point;
			Angle = angle;
		}
	}
}
