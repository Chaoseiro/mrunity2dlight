﻿using UnityEngine;
using System.Collections.Generic;

namespace LikeASir
{
	[RequireComponent(typeof(Camera))]
	[ExecuteInEditMode]
	public class MrLightCamera : MonoBehaviour
	{
		public Camera CachedCamera { get; protected set; }
		public Vector2[] ViewportWorldCoordinates { get; protected set; }
		public Vector3 CachedPosition { get; protected set; }

		MrLightObstacle _cameraBounds;
		EdgeCollider2D _cameraCollider;

		// Use this for initialization
		void OnEnable()
		{
			CachedCamera = GetComponent<Camera>();
			CachedPosition = transform.position;

			// Add the camera as an obstacle
			_cameraCollider = gameObject.GetComponent<EdgeCollider2D>();
			if(_cameraCollider == null)
				_cameraCollider = gameObject.AddComponent<EdgeCollider2D>();
			_cameraCollider.isTrigger = true;
			
			_cameraBounds = gameObject.GetComponent<MrLightObstacle>();
			if(_cameraBounds == null)
				_cameraBounds = gameObject.AddComponent<MrLightObstacle>();
			
			ViewportWorldCoordinates = new Vector2[5];
			Vector2 halfScreenSize = GetScreenSizeInWorld() / 2.0f;
			_cameraCollider.points = new Vector2[] 
			{
				new Vector2(-halfScreenSize.x - MrLightManager.Instance.CameraViewportExtension, -halfScreenSize.y - MrLightManager.Instance.CameraViewportExtension),
				new Vector2(halfScreenSize.x + MrLightManager.Instance.CameraViewportExtension, -halfScreenSize.y - MrLightManager.Instance.CameraViewportExtension),
				new Vector2(halfScreenSize.x + MrLightManager.Instance.CameraViewportExtension, halfScreenSize.y + MrLightManager.Instance.CameraViewportExtension),
				new Vector2(-halfScreenSize.x - MrLightManager.Instance.CameraViewportExtension, halfScreenSize.y + MrLightManager.Instance.CameraViewportExtension),
				new Vector2(-halfScreenSize.x - MrLightManager.Instance.CameraViewportExtension, -halfScreenSize.y - MrLightManager.Instance.CameraViewportExtension)
			};
			UpdateViewportCoordinates();
		}

		// Update is called once per frame
		void Update()
		{
			UpdateViewportCoordinates();
		}

		protected void UpdateViewportCoordinates()
		{
			if (MrLightManager.Instance == null)
				return;
			CachedPosition = transform.position;
			ViewportWorldCoordinates[0] = CachedCamera.ViewportToWorldPoint(new Vector2(-MrLightManager.Instance.CameraViewportExtension, -MrLightManager.Instance.CameraViewportExtension));
			ViewportWorldCoordinates[1] = CachedCamera.ViewportToWorldPoint(new Vector2(1 + MrLightManager.Instance.CameraViewportExtension, -MrLightManager.Instance.CameraViewportExtension));
			ViewportWorldCoordinates[2] = CachedCamera.ViewportToWorldPoint(new Vector2(1 + MrLightManager.Instance.CameraViewportExtension, 1 + MrLightManager.Instance.CameraViewportExtension));
			ViewportWorldCoordinates[3] = CachedCamera.ViewportToWorldPoint(new Vector2(-MrLightManager.Instance.CameraViewportExtension, 1 + MrLightManager.Instance.CameraViewportExtension));
			ViewportWorldCoordinates[4] = ViewportWorldCoordinates[0];
		}

		public bool IsObstacleIsInView(MrLightObstacle obstacle)
		{
			if (obstacle == null)
				return false;
			// if any of the obstacle's points is visible, it is considered in the calculations
			Vector2[] points = obstacle.GetPoints(CachedPosition);
			if (points == null)
				return true;
			for (int i = 0; i < points.Length; i++)
			{
				if (IsPointInView(points[i]))
					return true;
			}
			// lastly, if it is a big obstacle it might be in screen but not its points
			Plane[] planes = GeometryUtility.CalculateFrustumPlanes(CachedCamera);
			return GeometryUtility.TestPlanesAABB(planes, obstacle.LightCollider.GetBounds());
		}

		public bool IsPointInView(Vector2 point)
		{
			Vector2 AB = ViewportWorldCoordinates[1] - ViewportWorldCoordinates[0];
			Vector2 AP = point - ViewportWorldCoordinates[0];
			Vector2 BC = ViewportWorldCoordinates[2] - ViewportWorldCoordinates[1];
			Vector2 BP = point - ViewportWorldCoordinates[1];
			float dotABAP = Vector2.Dot(AB, AP);
			float dotABAB = Vector2.Dot(AB, AB);
			float dotBCBP = Vector2.Dot(BC, BP);
			float dotBCBC = Vector2.Dot(BC, BC);
			return (0 <= dotABAP && dotABAP <= dotABAB) &&
				(0 <= dotBCBP && dotBCBP <= dotBCBC);
		}

		public float RayMaxNeededDistance()
		{
			Vector2 screenSize = GetScreenSizeInWorld();
			return Mathf.Sqrt(screenSize.x * screenSize.x + screenSize.y + screenSize.y);
		}

		public Vector2 GetScreenSizeInWorld()
		{
			if (CachedCamera == null)
				return Vector2.zero;
			float height = CachedCamera.orthographicSize;
			float width = height * CachedCamera.aspect;
			return new Vector2(2 * width, 2 * height);
		}

		public Vector2[] GetCollisionPointsWithViewBox(Vector2 lineStart, Vector2 lineEnd)
		{
			List<Vector2> points = new List<Vector2>();
			
			for (int i = 1; i < ViewportWorldCoordinates.Length; i++)
			{
				points.Add(MathfExtensions.LineToLineIntersection(
					ViewportWorldCoordinates[i - 1], ViewportWorldCoordinates[i],
					lineStart, lineEnd));
					
			}
			return points.ToArray();
		}
	}
}