﻿using UnityEngine;
using System.Collections.Generic;

namespace LikeASir
{
	public class MrRadialLightSource : MrLightSource
	{
		public float MaxLightRayDistance = 3.0f;
		public float RadialDegreePrecision = 1.0f;
		[Range(0, 360)]
		public float ConeStartAngle = 0;
		[Range(0, 360)]
		public float ConeEndAngle = 360;

		void Awake()
		{
			Initialize();
		}

		protected override void LateUpdate()
		{
			base.LateUpdate();
		}

		public override void DrawLight()
		{
			List<IntersectionPoint> intersectionPoints = CalculateIntersections();
			if (intersectionPoints == null)
				return;

			List<Vector3> meshVertices = new List<Vector3>();
			List<int> triangles = new List<int>();
			int currentVertexIndex = -1;
			int previousVertexIndex = -1;

			meshVertices.Add(Vector3.zero);
			previousVertexIndex = 0;
			for (int i = 0; i < intersectionPoints.Count; i++)
			{
				meshVertices.Add(intersectionPoints[i].Point - CachedPosition);
				previousVertexIndex = currentVertexIndex;
				currentVertexIndex = meshVertices.Count - 1;
				if (i > 0)
					AddTriangle(triangles, 0, currentVertexIndex, previousVertexIndex);
			}
			if(ConeEndAngle - ConeStartAngle == 360)
				AddTriangle(triangles, 0, 1, currentVertexIndex);

			DeployMesh(meshVertices, triangles);
		}

		public override List<IntersectionPoint> CalculateIntersections()
		{
			if (LightCamera == null || MrLightManager.Instance == null)
				return null;

			if (ConeEndAngle < ConeStartAngle)
			{
				float c = ConeEndAngle;
				ConeEndAngle = ConeStartAngle;
				ConeStartAngle = c;
			}
			List<MrLightObstacle> obstacles = MrLightManager.Instance.Obstacles;
			List<Vector2> raycastPoints = new List<Vector2>();
			List<Vector2> raycastDirections = new List<Vector2>();
			List<IntersectionPoint> intersectionPoints = new List<IntersectionPoint>();
			RaycastHit2D hit2D;

			if (RadialDegreePrecision <= 0 || RadialDegreePrecision >= 360)
				RadialDegreePrecision = 1;

			// First list objects that will be considered for this light, that are close enough
			List<MrLightObstacle> closeObstacles = new List<MrLightObstacle>();
			if (ObstacleLayers.value != 0)
			{
				for (int i = 0; i < obstacles.Count; i++)
				{
					// if the obstacle is not visible to the camera, wont use it for raycasts
					if (LightCamera != null && !LightCamera.IsObstacleIsInView(obstacles[i]))
						continue;

					if (ObstacleLayers == (ObstacleLayers | 1 << obstacles[i].LightCollider.gameObject.layer))
					{
						Vector2[] points = obstacles[i].GetPoints(CachedPosition);
						Vector2 obsPos = obstacles[i].transform.position;
						for (int j = 0; j < points.Length; j++)
						{
							Vector2 pos = obsPos + points[j];
							if ((CachedPosition - pos).sqrMagnitude <= (2 * MaxLightRayDistance) * MaxLightRayDistance)
							{
								closeObstacles.Add(obstacles[i]);
								break;
							}
						}
					}
				}
			}

			// Add RaycastDirections from cone
			for (float angle = ConeStartAngle; angle <= ConeEndAngle; angle += RadialDegreePrecision)
			{
				float radAngle = angle * Mathf.Deg2Rad;
				if (ObstacleLayers.value != 0)
				{
					raycastDirections.Add(new Vector2(Mathf.Cos(radAngle), Mathf.Sin(radAngle)));
				}
				else
				{
					Vector2 intersectPoint = CachedPosition + MaxLightRayDistance * new Vector2(Mathf.Cos(radAngle), Mathf.Sin(radAngle));
					intersectionPoints.Add(new IntersectionPoint(intersectPoint, angle));
				}
			}

			if (ObstacleLayers.value != 0)
			{
				for (int i = 0; i < closeObstacles.Count; i++)
				{
					// Add object's points to raycast list
					Vector2[] points = closeObstacles[i].GetPoints(CachedPosition);
					if (points != null)
					{
						raycastPoints.AddRange(points);
					}
				}

				// convert the raycast points into directions
				for (int i = 0; i < raycastPoints.Count; i++)
				{
					Vector2 dif = (raycastPoints[i] - CachedPosition);
					float angle = Mathf.Atan2(dif.y, dif.x);//Vector2.Angle(CachedPosition, raycastPoints[j]) * Mathf.Deg2Rad;
					// check if this angle is inside view cone
					//if (angle >= ConeStartAngle * Mathf.Deg2Rad && angle <= ConeEndAngle * Mathf.Deg2Rad)
					//{
					raycastDirections.Add(new Vector2(Mathf.Cos(angle - MrLightManager.Instance.RaycastDeltaAngle), Mathf.Sin(angle - MrLightManager.Instance.RaycastDeltaAngle)));
					raycastDirections.Add(new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)));
					raycastDirections.Add(new Vector2(Mathf.Cos(angle + MrLightManager.Instance.RaycastDeltaAngle), Mathf.Sin(angle + MrLightManager.Instance.RaycastDeltaAngle)));
					//}
				}

				// now, finally, raycast in the directions and save the intersection points
				for (int j = 0; j < raycastDirections.Count; j++)
				{
					hit2D = Physics2D.Raycast(CachedPosition, raycastDirections[j], MaxLightRayDistance, ObstacleLayers);
					if (hit2D.collider != null)
					{
						Vector2 intersectPoint = hit2D.point;
						float intersectAngle = Mathf.Atan2(intersectPoint.y - CachedPosition.y, intersectPoint.x - CachedPosition.x);
						//MrLightObstacle obstacle = hit2D.collider.gameObject.GetComponent<MrLightObstacle>();
						//if (obstacle != null)
						//	intersectPoint += obstacle.LightPenetration * raycastDirections[j];
						intersectionPoints.Add(new IntersectionPoint(intersectPoint, intersectAngle));
					}
					else
					{
						Vector2 intersectPoint = CachedPosition + MaxLightRayDistance * raycastDirections[j];
						float intersectAngle = Mathf.Atan2(intersectPoint.y - CachedPosition.y, intersectPoint.x - CachedPosition.x);
						intersectionPoints.Add(new IntersectionPoint(intersectPoint, intersectAngle));
					}
				}
			}
			
			if (intersectionPoints.Count < 1)
				return null;

			// sort intersections by angle
			intersectionPoints.Sort(
				delegate(IntersectionPoint i1, IntersectionPoint i2)
				{
					return i1.Angle.CompareTo(i2.Angle);
				}
				);

			//return intersectionPoints;
			// now reduce intersection points by removing points that are too close
			List<IntersectionPoint> reducedIntersections = new List<IntersectionPoint>();
			reducedIntersections.Add(intersectionPoints[0]);
			for (int i = 1; i < intersectionPoints.Count; i++)
			{
				if ((intersectionPoints[i].Point - reducedIntersections[reducedIntersections.Count - 1].Point).sqrMagnitude < 0.0005f)
					continue;
				reducedIntersections.Add(intersectionPoints[i]);
			}

			return reducedIntersections;
		}

		protected override float GetMaxLightLength()
		{
			return MaxLightRayDistance;
		}
	}
}