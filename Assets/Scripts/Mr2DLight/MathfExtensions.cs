﻿using UnityEngine;

namespace LikeASir
{
	public static class MathfExtensions
	{
		public static Vector2 LineToLineIntersection(Vector2 lineStart1, Vector2 lineEnd1, Vector2 lineStart2, Vector2 lineEnd2)
		{
			Vector2 lineDirection1 = lineEnd1 - lineStart1;
			Vector2 lineDirection2 = lineEnd2 - lineStart2;

			Vector2 q = lineStart1;
			Vector2 s = lineDirection1;

			Vector2 p = lineStart2;
			Vector2 r = lineDirection2;

			// The intersection is where q + u*s == p + t*r, and 0 <= u <= 1 && 0 <= t
			// t = (q − p) × s / (r × s)
			// u = (q − p) × r / (r × s)

			float crossRS = CrossProduct2D(r, s);
			float crossQP_S = CrossProduct2D(q - p, s);
			float crossQP_R = CrossProduct2D(q - p, r);

			if (crossRS == 0)
			{
				// TODO: other situations
				
			}
			else
			{
				float t = crossQP_S / crossRS;
				float u = crossQP_R / crossRS;

				if ((0 <= t && t <= 1) && (0 <= u && u <= 1))
				{
					return q + u * s;
				}
			}
			return Vector2.zero;
		}

		public static float CrossProduct2D(Vector2 p, Vector2 q)
		{
			return p.x * q.y - p.y * q.x;
		}
	}
}
