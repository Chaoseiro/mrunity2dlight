Shader "Masked/Mask" {
    Properties {
		_MainTex ("Alpha (A) only", 2D) = "white" {} 
	}
    SubShader {
        // Render the mask after regular geometry, but before masked geometry and
        // transparent things.
        
        Tags {"Queue" = "Geometry+10" }
        
        // Don't draw in the RGBA channels; just the depth buffer
        
        ColorMask RGB
        ZWrite On
        
        // Do nothing specific in the pass:
        
        Pass {
		Blend OneMinusSrcAlpha SrcAlpha
			SetTexture [_MainTex] {
				combine texture * primary
			}
		}
    }
}