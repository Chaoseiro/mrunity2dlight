# MrUnity2DLight README #

This project is a simple 2D light for Unity 4.3+

It is based on the following resources:

* https://github.com/unitycoder/2DShadow
* http://www.redblobgames.com/articles/visibility/
* http://ncase.me/sight-and-light/

And includes the following:

* http://wiki.unity3d.com/index.php/DepthMask
* http://unitypatterns.com/singletons/